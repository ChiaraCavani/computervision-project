import cv2 as cv
import pandas as pd


#dataset's size: 95
#if the number of paintings change, edit this parameter
DIM_DATASET=95

def read_db(img):
    img_db = cv.imread(img)
    #plt.imshow(img_db), plt.show()
    # DETECTION DATABASE WITH ORB
    orb = cv.ORB_create()
    # find the keypoints with ORB
    kp_db, des_db = orb.detectAndCompute(img_db,None)
    return kp_db, des_db

# SAVE DESCRIPTOR
def descrittori_DB ():
    descrittori = {}
    for j in range(DIM_DATASET):
        kp_db, des_db=read_db("paintings_db/" + str(j).zfill(3) + ".png") #zfill to deal with 3 digits (000)
        descrittori[j]=des_db
    return descrittori

# RETRIEVAL FUNCTION
def recognize_paiting(img, desc):

    MIN_MATCH_COUNT = 0
    # DETECTION WITH ORB
    orb = cv.ORB_create()
    # find the keypoints with ORB
    kp, des = orb.detectAndCompute(img,None)

    # create BFMatcher object
    bf = cv.BFMatcher()
    results={}
    #filter on database
    for i in range (DIM_DATASET):

        # Match descriptors
        matches = bf.knnMatch(desc[i],des, k=2)
        #print(matches)
        good = []
        for m, n in matches:
            if m.distance < 0.75 * n.distance:
                good.append([m])

        # check lenght>0
        # make a dictionary [img,len] and sort it
        if len(good) > MIN_MATCH_COUNT:
           results[str(i).zfill(3) + ".png"] = len(good)
    # find the firsts best matches
    # first one should be the best
    sorted_x = sorted(results.items(), key=lambda kv: kv[1], reverse=True)
    print("List of paintings matched:")
    print(sorted_x)

    image = cv.imread("paintings_db/"+ sorted_x[0][0])

    dataset= pd.read_csv("data.csv", sep=',')
    print("The paint with more matches is:")
    row = dataset.loc[dataset['Image'] == sorted_x[0][0]]
    print('Title: ' + str(row['Title'].values[0]))
    print('Author: ' + str(row['Author'].values[0]))
    print('Room: ' + str(row['Room'].values[0]))

    return image, int(row['Room'].values[0])