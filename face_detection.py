import cv2
import sys
import logging as log
import datetime as dt
from time import sleep

def detect_faces(video):
    cascPath = "haarcascade_frontalface_default.xml"
    faceCascade = cv2.CascadeClassifier(cascPath)
    eye_cascade = cv2.CascadeClassifier('haarcascade_eye.xml')
    #log.basicConfig(filename='webcam.log', level=log.INFO)

    RESIZE = 0.7

    video_capture = cv2.VideoCapture(video)
    anterior = 0

    # Check if camera opened successfully
    if (video_capture.isOpened() == False):
        print("Error opening video stream or file")

    while(video_capture.isOpened()):
        # Capture frame-by-frame
        ret, frame = video_capture.read()

        try:
            reduced = cv2.resize(frame, None, fx=RESIZE, fy=RESIZE, interpolation=cv2.INTER_LINEAR)
        except:
            break

        gray = cv2.cvtColor(reduced, cv2.COLOR_BGR2GRAY)

        faces = faceCascade.detectMultiScale(
            gray,
            scaleFactor=1.1,
            minNeighbors=14,
            minSize=(30, 30)
        )

        try:
            if not faces:
                print("The person is facing the paintings")
        except ValueError:
            pass

        # Draw a rectangle around the faces
        for (x, y, w, h) in faces:
            cv2.rectangle(reduced, (x, y), (x + w, y + h), (0, 200, 250), 2)
            roi_gray = gray[y:y + h, x:x + w]
            roi_color = reduced[y:y + h, x:x + w]

            eyes = eye_cascade.detectMultiScale(roi_gray)
            for (ex, ey, ew, eh) in eyes:
                if len(eyes) != 0:
                    cv2.rectangle(roi_color, (ex, ey), (ex + ew, ey + eh), (250, 100, 0), 2)
                    print("The person is not facing the paintings")

        if anterior != len(faces):
            anterior = len(faces)
            log.info("faces: "+str(len(faces))+" at "+str(dt.datetime.now()))


        # Display the resulting frame
        cv2.imshow('Video', reduced)


        if cv2.waitKey(25) & 0xFF == ord('q'):
            break

    # When everything is done, release the capture
    video_capture.release()
    cv2.destroyAllWindows()


if __name__ == '__main__':
    print("\nLoading face detector...")
    video = "people.mp4"

    detect_faces(video)