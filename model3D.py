import sys
import cv2 as cv
import numpy as np
import random as rnd

from orb import recognize_paiting, descrittori_DB

def fillflood_painting(img, flag):
    flooded = img.copy()
    fH = flooded.shape[0]
    fW = flooded.shape[1]
    # Mask should be 2 pixels wider and taller than image 
    mask = np.zeros((fH+2, fW+2), np.uint8) 
    max = 0

    for y in range(0, fH):
        for x in range(0, fW):
             # Fill the mask if it finds an area with the same color
            if mask[y + 1][x + 1] == 0:
                # Fill the area with rand color
                pixel = (rnd.randint(0, 256), rnd.randint(0, 256), rnd.randint(0, 256))

                _, _, _, out = cv.floodFill(flooded, mask, (x, y), pixel, (9, 10, 9), (35, 35, 35), flag)

                size = out[2] * out[3] # This is the size of the homogenous area found before
                
                if size > max:
                    # With this condition the background will be the largest area
                    max = size  
                    # Assign to the background the area defined before
                    background = pixel

    # Inside the input image fill the wall (largest area) with "background"
    fill = cv.inRange(flooded, background, background)
    return fill

def AntiPerspective(img, back, points):

    sum = points.sum(axis=1)
    vert = np.diff(points, axis=1)

    " Calculate the 4 vertex "
    tl = points[np.argmin(sum)]
    br = points[np.argmax(sum)]
    tr = points[np.argmin(vert)]
    bl = points[np.argmax(vert)]

    " Add a point on each vertex found "
    cv.circle(back, (tl[0], tl[1]), 3, (0, 0, 255), -1)
    cv.circle(back, (tr[0], tr[1]), 3, (0, 0, 255), -1)
    cv.circle(back, (bl[0], bl[1]), 3, (0, 0, 255), -1)
    cv.circle(back, (br[0], br[1]), 3, (0, 0, 255), -1)

    " Original image "
    size = img.shape
    " Vector of source points "
    pts_src = np.array(
        [
            [0, 0],
            [size[1] - 1, 0],
            [size[1] - 1, size[0] - 1],
            [0, size[0] - 1]
        ], dtype=float
    );

    " Vector of destination points "
    pts_dst = np.float32([[tl[0], tl[1]], [tr[0], tr[1]], [br[0], br[1]], [bl[0], bl[1]]])

    " Find Homography between the source and the destination vectors "
    matrix, status = cv.findHomography(pts_src, pts_dst);

    " Apply the perspective "
    im_temp = cv.warpPerspective(img, matrix, (back.shape[1],back.shape[0]))

    " Fill the rectangular found with a black background in order to obtain a better result in the next step "
    cv.fillConvexPoly(back, pts_dst.astype(int), 0, 16);

    " Add warped source image to destination image "
    im_dst = back + im_temp;

    " Display image "
    cv.imshow("Image", im_dst);


    return im_dst

def Model3D (frame):

    " 1. RESIZE FRAME "
    if (frame.shape[0] > 2000 or frame.shape[1] > 2000):
        reduced = cv.resize(frame, None, fx=.3, fy=.3)
    else:
        reduced = cv.resize(frame, None, fx=.8, fy=.8)

    " 2. CREATE A COPY AND A CLEAN COPY "
    out = reduced
    clear = out.copy()

    " 3. NORMALIZE "
    n = cv.normalize(out, None, 0, 255, cv.NORM_MINMAX)

    " 4. FLOODFILL "
    dst_4 = fillflood_painting(n, 8)
    image = cv.bitwise_not(dst_4)
    #cv.imshow("FILLFLOOD", image)

    " 5. MORPHOLOGY "
    kernel = np.ones((3, 3), np.uint8)
    dil = cv.dilate(image, kernel, iterations=1)
    erosion = cv.erode(dil, kernel, iterations=1)
    mor = cv.morphologyEx(erosion, cv.MORPH_OPEN, kernel)
    #cv.imshow("MOR", mor)


    " 6. DETECT CONTOURS "
    contours, hierarchy = cv.findContours(mor, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
    contours = sorted(contours, key=cv.contourArea, reverse=True)[:10]
    reduce = 1


    points_list = list()
    paint = list()
    # Used for larger videos to avoide small areas
    if (np.shape(out)[1] > 1000):
        reduce = 2

    " WORK ON CONTOURS TO DRAW THEM "
    for i, contour in enumerate(contours):
        epsilon = 0.05 * cv.arcLength(contour, True)
        pol = cv.approxPolyDP(contour, epsilon, True)

        # If pol have 4 points it can be a paint to detect
        if len(pol) == 4 and cv.isContourConvex(pol) and cv.contourArea(pol) > (1500 * reduce) and cv.contourArea(pol) < (200000 * reduce):

            " APPROXIMATE CONTOURS "
            points_list.append(pol[:, 0, :])
            (x, y, w, h) = cv.boundingRect(pol)
            paint.append((x, y, w, h))

            "Se voglio i rettangoli generali"
            cv.rectangle(out, (x, y), (x + w, y + h), (0, 255, 0), 1)

            " DRAW CONTOURS "
            cv.drawContours(out, [pol], -1, (0, 255, 0), 2)

    # Number of paintings found
    n=len(paint)
    for i in range(n):
        x = paint[i][0]
        y = paint[i][1]
        w = paint[i][2]
        h = paint[i][3]
        " Crop the initial image "
        frame = clear[y:y + h, x:x + w]
        " Increase the size of the frame in order to obtain a better result in the retrival branch "
        frame = cv.resize(frame, None, fx=2.5, fy=2.5)

        " Identify the paintings found with a number "
        cv.putText(out, str(i), (x,y), cv.FONT_HERSHEY_DUPLEX, 1, (255, 255, 255), 1, cv.LINE_AA)

        " Retrival branch "
        img = recognize_paiting(frame, desc_DB)[0]

        " Calculate the inverse perspective and fill the image "
        clear = AntiPerspective(img, clear,points_list[i])


    # SHOW IMAGE DETECT
    position_text = (5, out.shape[0] - 10)
    cv.putText(out, "PAINTS DETECTED:" + str(len(points_list)), position_text, cv.FONT_HERSHEY_DUPLEX, 1, (255, 255, 255), 1, cv.LINE_AA)
    cv.imshow('DETECT PAINT', out)



if __name__ == '__main__':

    # Load the descriptors of the images inside the DB
    print("Loading DB...")
    desc_DB = descrittori_DB()

    " Insert HERE the images to compute "
    img = "screenshots_3d_model/89787668_212621483428698_8705798958376550400_n.jpg"
    img2 = "screenshots_3d_model/90006902_532419750609250_114822239806816256_n.png"
    print('Starting processing the image:', img)

    # Read the image
    frame = cv.imread(img)

    " Call this function to DETECT paintings and then to fill with the good images of the DB "
    Model3D(frame)

    cv.waitKey(0)
    sys.exit()  # exit the program
