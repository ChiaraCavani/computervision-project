import sys
import cv2 as cv
import numpy as np
import os
import random as rnd

from yolov3.detect import *
from face_detection import *

from orb import recognize_paiting, descrittori_DB

RESIZE_DIM = 0.4
room = 1


def fillflood_painting(img, flag):
    flooded = img.copy()
    fH = flooded.shape[0]
    fW = flooded.shape[1]
    # Mask should be 2 pixels wider and taller than image 
    mask = np.zeros((fH+2, fW+2), np.uint8)  
    max = 0

    for y in range(0, fH):
        for x in range(0, fW):
            # Fill the mask if it finds an area with the same color
            if mask[y + 1][x + 1] == 0:
                pixel = (rnd.randint(0, 256), rnd.randint(0, 256), rnd.randint(0, 256))   # Fill the area with rand color

                _, _, _, out = cv.floodFill(flooded, mask, (x, y), pixel, (3, 3, 3), (12, 12, 12), flag)

                size = out[2] * out[3]    # This is the size of the homogenous area found before
                if size > max:
                    # With this condition the background will be the largest area
                    max = size  
                    # Assign to the background the area defined before
                    background = pixel

    # Inside the input image fill the wall (largest area) with "background"
    fill = cv.inRange(flooded, background, background) 
    return fill

def RectifyPaint(frame, points):
    r = False
    sum = points.sum(axis=1)
    diff = np.diff(points, axis=1)

    " Calculate the 4 vertex "
    tl = points[np.argmin(sum)]
    br = points[np.argmax(sum)]
    tr = points[np.argmin(diff)]
    bl = points[np.argmax(diff)]

    width1 = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    width2 = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    if (width1 == 0 or width2 == 0):
        r = True
    maxWidth = max(int(width1), int(width2))
    
    height1 = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    height2 = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    if (height1 == 0 or height2 == 0):
        r = True
    maxHeight = max(int(height1), int(height2))

    " This condition is used for paintings not rectangular (rombi)"
    if (r == True):
        tl = points[1]
        tr = points[0]
        br = points[3]
        bl = points[2]

        width1 = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
        width2 = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
        maxWidth = max(int(width1), int(width2))

        height1 = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
        height2 = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
        maxHeight = max(int(height1), int(height2))

    if cv.contourArea(points) > 2000:
        dst = np.array([[0, 0], [maxWidth - 1, 0], [maxWidth - 1, maxHeight - 1], [0, maxHeight - 1]], dtype=np.float32)
        M = cv.getPerspectiveTransform(np.array([tl, tr, br, bl]).astype(np.float32), dst)
        warped = cv.warpPerspective(frame, M, (maxWidth, maxHeight))

        # Resize frame to improve quality
        size_final = 400
        x_original = warped.shape[1]
        y_original = warped.shape[0]

        alpha = float(size_final) / max(x_original, y_original)
        final_y = int(y_original * alpha)
        final_x = int(x_original * alpha)

        resized = cv.resize(warped, (final_x, final_y))

        cv.imshow("PAINT RECTIFIED", resized)

    return resized


def onMouse(event, x, y, int, par):
    if (event == cv.EVENT_LBUTTONDOWN):
        frame = par[0]
        points = par[1]
        rect = par[2]
        dim = len(par[2])

        for i in range(dim):
            x_rect = rect[i][0]
            y_rect = rect[i][1]
            w_rect = rect[i][2]
            h_rect = rect[i][3]
            # If the user click inside the bounding box:
            if (x > x_rect and x < (x_rect + w_rect)) and (y > y_rect and y < (y_rect + h_rect)):
                # Original image cropped
                imgcrop = frame[y_rect:y_rect+h_rect, x_rect:x_rect+w_rect]
                cv.imshow('ORIGINAL PAINT', imgcrop)

                print("Starting the RECTIFICATION of the paint clicked...")
                rect = RectifyPaint(frame, points[i])

                print("Starting the RETRIVAL of the paint...")
                global room
                room = recognize_paiting(rect, desc_DB)[1]

                if cv.waitKey(0) & 0xFF == ord('c'):
                    pass
                return room


def DetectPaint(frame):

    # 1. REDUCE SIZE OF THE FRAME  (to make the algorithm faster)
    if frame.shape[1] > 2500 or frame.shape[0] > 2500:
        RESIZE = 0.25
    elif (frame.shape[1] > 1100 or frame.shape[0] > 1100) and (frame.shape[1] < 2500 or frame.shape[0] < 2500):
        RESIZE = 0.4
    elif frame.shape[1] < 1100 or frame.shape[0] < 1100:
        RESIZE = 0.55
    else:
        RESIZE = 0.4

    reduced = cv.resize(frame, None, fx=RESIZE, fy=RESIZE, interpolation=cv.INTER_LINEAR)

    # 2. CREATE A COPY AND A CLEAN COPY
    out = reduced
    clear = out.copy()

    # 3. MEAN
    "the output of the function is the filtered “posterized” image"
    mean = cv.pyrMeanShiftFiltering(out, 5, 15, 1)
    #cv.imshow("M",mean)

     # 4. NORMALIZE
    n = cv.normalize(mean, None, 0, 255, cv.NORM_MINMAX)

    # 5. APPLY FLOODFILL TO IMPROVE THE DETECTION ( with 4-connectivity )
    dst_4 = fillflood_painting(n, 4)

    # 6. Invert floodfilled image
    image = cv.bitwise_not(dst_4)
    #cv.imshow("FILLFLOOD", image)

    # 7. MORPHOLOGY
    kernel = np.ones((3, 3), np.uint8)
    dil = cv.dilate(image, kernel, iterations=1)
    # cv.imshow('dil', dil)
    erosion = cv.erode(dil, kernel, iterations=1)
    # cv.imshow("ER", erosion)
    mor = cv.morphologyEx(erosion, cv.MORPH_OPEN, kernel)
    #cv.imshow("MOR", mor)


    # DETECT CONTOURS
    contours, hierarchy = cv.findContours(mor, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
    contours = sorted(contours, key=cv.contourArea, reverse=True)[:10]
    reduce = 1

    points_list = list()
    paint = list()
    
    # Used for larger videos to avoide small areas
    if (np.shape(out)[1] > 1000):
        reduce = 2

    if (np.shape(out)[1] > 2000):
        reduce = 3

    # WORK ON CONTOURS TO DRAW THEM
    for i, contour in enumerate(contours):
        epsilon = 0.05 * cv.arcLength(contour, True)
        pol = cv.approxPolyDP(contour, epsilon, True)

        # If pol have 4 points it can be a paint to detect
        if len(pol) == 4 and cv.isContourConvex(pol) and cv.contourArea(pol) > (4000 * reduce) and cv.contourArea(pol) < (200000 * reduce):

            # APPROXIMATE CONTOURS
            points_list.append(pol[:, 0, :])
            (x, y, w, h) = cv.boundingRect(pol)
            paint.append((x, y, w, h))
            
            "General bounding box"
            cv.rectangle(out, (x, y), (x + w, y + h), (0, 255, 0), 1)

            # DRAW CONTOURS (a bounding box more precise)
            cv.drawContours(out, [pol], -1, (0, 255, 0), 2)

            # Call the function "OnMouse" after the user has clicked over the paint
            cv.setMouseCallback("DETECT PAINT", onMouse, [clear, points_list, paint])


    # SHOW VIDEO DETECT
    position_text = (5, out.shape[0] - 10)
    cv.putText(out, "PAINTS DETECTED:" + str(len(points_list)), position_text, cv.FONT_HERSHEY_DUPLEX, 1, (255, 255, 255), 1, cv.LINE_AA)
    cv.imshow('DETECT PAINT', out)


def video_process(video):
    print('Starting processing the video:', video)

    # Capture a video frame-by-frame
    cap = cv.VideoCapture(video)
    # Variable use to skip frames
    ct = 0

    # Check if camera opened successfully
    if (cap.isOpened() == False):
        print("Error opening video stream or file")

    # Read until video is completed
    while(cap.isOpened()):
        ct += 1
        ret = cap.grab()
        if ct % 5 == 0:
            # skip some frames
            ret, frame = cap.retrieve()

            # SEGMENTATION & DETECTION
            try:
                DetectPaint(frame)
            except AttributeError:
                break

            # Press Q on keyboard to stop the video
            if cv.waitKey(25) & 0xFF == ord('q'):
                break

    # When everything done, release the capture
    cap.release()
    cv.destroyAllWindows()


if __name__ == '__main__':

    # Load the descriptors of the images belonging to the DB
    print("Loading DB...")
    desc_DB = descrittori_DB()

    # Video good both for painting detection and people localization
    video = "GOPR5828.MP4"

    # Video good for painting detection (no people inside)
    video2 = "20180206_113800.mp4"
    video3 = "GOPR5826.MP4"

    # Video good both for people detection (no retrieval)
    video4 = "people.mp4"


    # Start the painting process
    video_process(video)

    # People detection and localization
    print("\nLoading people detector...")
    num_people = yolo_detect(video, room)

    print(f"People found: {num_people}")

    cv.waitKey(0)
    sys.exit()  # exit the program

